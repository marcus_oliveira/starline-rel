# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse

from dashplatform import models


def view_report(request, **kwargs):
    querystring = request.GET.urlencode()

    report_id = get_report_id_from_request(request.GET)

    report = get_report_by_id(report_id)

    published = get_or_publish_report(report, querystring)

    return HttpResponse(
        "Publicação - QueryString: " + published.query_string + " /// Report: " + report.name)


def get_report_id_from_request(query_dict):
    report_id = query_dict.get('report_id')

    if not report_id:
        raise Exception("report_id não informado")

    return report_id


def get_report_by_id(report_id):
    try:
        report = models.Report.objects.get(pk=report_id)
    except models.Report.DoesNotExist:
        raise Exception("Report não existe")

    return report


def get_or_publish_report(report, query_string):
    try:
        return models.PublishedReport.objects.get(query_string=query_string, report=report)
    except models.PublishedReport.DoesNotExist:
        return models.PublishedReport.objects.create(query_string=query_string, report=report)
# TODO: Preencher aqui a url_report
