# -*- coding: UTF-8 -*-
from django.conf.urls import url

from . import views

urlpatterns = [
    url('^view_report/$', views.view_report, name='view_report')
]
