# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from dashplatform import signals

# TODO: Mudar isso para fazer upload da amazon


class Product(models.Model):
    name = models.CharField(max_length=25)

    def __unicode__(self):
        return self.name


class Report(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True, max_length=500)
    product = models.ForeignKey(Product)
    report_file = models.FileField()

    def __unicode__(self):
        return self.name


models.signals.pre_save.connect(signals.check_pbix_file, Report, dispatch_uid='check_pbix_file')


class PublishedReport(models.Model):
    report = models.ForeignKey(Report)
    query_string = models.CharField(max_length=255)
    report_url = models.CharField(blank=True, null=True, max_length=500)
