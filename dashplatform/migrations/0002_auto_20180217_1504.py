# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-02-17 15:04
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dashplatform', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=25)),
            ],
        ),
        migrations.CreateModel(
            name='ReportPublication',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('query_string', models.CharField(max_length=255)),
                ('report_url', models.CharField(max_length=500)),
            ],
        ),
        migrations.RemoveField(
            model_name='report',
            name='product_key',
        ),
        migrations.AddField(
            model_name='reportpublication',
            name='report',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dashplatform.Report'),
        ),
        migrations.AddField(
            model_name='report',
            name='product',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='dashplatform.Product'),
            preserve_default=False,
        ),
    ]
