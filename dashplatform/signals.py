# -*- encoding: utf-8 -*-


def check_pbix_file(sender, instance, **kwargs):
    if not instance.report_file.file.name.endswith('pbix'):
        raise Exception("Arquivo inválido (não é do tipo pbix)")
