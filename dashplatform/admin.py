# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from dashplatform import models as dashplatform_models

admin.site.register(dashplatform_models.Product)
admin.site.register(dashplatform_models.Report)
admin.site.register(dashplatform_models.PublishedReport)
